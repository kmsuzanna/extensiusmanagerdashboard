﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;

public class CardManager : Singleton<CardManager>
{
    public Sprite[] CardDatabase;

    public enum CardType
    {
        Associates,
        Capacity,
        Inbound,
        Inventory,
        Outbound
    }


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
}
