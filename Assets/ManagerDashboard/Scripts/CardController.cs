﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity.InputModule;

public class CardController : MonoBehaviour
{
    private bool isFilled;
    private Image image;
    private CardPlaceable cardPlaceable;
    private DashboardManager dashboardManager;
    Sprite originalCard;

    private GameObject movingCardContainer;

    public void Initialize(DashboardManager dashboardManager)
    {
        isFilled = false;
        image.sprite = null;
        image.enabled = false;
        cardPlaceable.enabled = false;

        //InitImage();
        this.dashboardManager = dashboardManager;
        movingCardContainer = dashboardManager.CardBeingPlacedContainer;

    }

    public void AddCard(Sprite sprite)
    {
        image.enabled = true;
        originalCard = sprite;
        image.color = new Color(255, 255, 255, 255);
        image.sprite = sprite;
        isFilled = true;
        cardPlaceable.enabled = true;
        cardPlaceable.HostTransform = dashboardManager.CardBeingPlacedContainer.transform;
    }


    public Sprite RemoveCard()
    {
        isFilled = false;

        Sprite sprite = image.sprite;
        InitImage();
        image.enabled = false;
        return sprite;
    }

    private void InitImage()
    {

        //image.sprite = null;
        //image.color = new Color(0, 0, 0, 0);
    }

    private void Awake()
    {
        image = GetComponent<Image>();
        cardPlaceable = GetComponent<CardPlaceable>();
        cardPlaceable.OnCardPlace += OnPlaceCard;
        cardPlaceable.OnCardSelect += OnSelectCard;

        cardPlaceable.enabled = false;

    }
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPlaceCard()
    {
        GameObject hitCard;
        //image.sprite 
        RaycastHit hitInfo;
        //Sprite s = dashboardManager.DoneMovingCard();
        //return;
        bool hit = Physics.Raycast(movingCardContainer.transform.position, movingCardContainer.transform.forward, out hitInfo, .2f);
        if (hit)
        {
            hitCard = hitInfo.collider.gameObject;
            //If hit card is empty cancel
            CardController cc = hitCard.GetComponentInChildren<CardController>();
            Sprite s = dashboardManager.DoneMovingCard();

            if (cc.isFilled)
            {
                AddCard(originalCard);

            }
            else
            {
                cardPlaceable.enabled = false;
                cc.AddCard(originalCard);

            }
            //else move to current position
            //Sprite s = dashboardManager.DoneMovingCard();
        }
        else
        {
            AddCard(originalCard);
            //Cancel move
        }
    }


    public void OnSelectCard()
    {
        dashboardManager.InitMovingCard(this.transform.position,image.sprite, this.gameObject);
        RemoveCard();
    }

    public void SetSelectable(bool isSelectable)
    {
        if (isSelectable)
        {
            if (isFilled)
                cardPlaceable.enabled = true;
            else
                cardPlaceable.enabled = false;

        }
        else
            cardPlaceable.enabled = false;

    }


}
