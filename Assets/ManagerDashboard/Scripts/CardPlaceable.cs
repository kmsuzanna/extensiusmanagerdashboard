﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using UnityEngine.UI;
using HoloToolkit.Unity.InputModule;

public class CardPlaceable : MonoBehaviour, IFocusable, ISourceStateHandler, IInputHandler
{

    [Tooltip("The physics layer for cards to stick to.")]
    public int PhysicsLayer = 8;
    /// <summary>
    /// Event triggered when card is selected with a pinch.
    /// </summary>
    public event Action OnCardSelect;

    /// <summary>
    /// Event triggered when card is placed.
    /// </summary>
    public event Action OnCardPlace;

    public Transform HostTransform;
    public int LayerMask
    {
        get { return (1 << PhysicsLayer); }
    }

    private bool isDragging;

    private uint currentInputSourceId;
    private IInputSource currentInputSource;
    private Quaternion gazeAngularOffset;

    private float defaultZ;

    private Image imageSprite;

    private Bounds bounds;
    void IFocusable.OnFocusEnter()
    {
    }

    void IFocusable.OnFocusExit()
    {
    }

    void IInputHandler.OnInputDown(InputEventData eventData)
    {
        if (isDragging) return;
        defaultZ = this.transform.localPosition.z - 0.2f;

        //dashboardManager.imageSprite.sprite = imageSprite.sprite;
        //imageSprite.sprite = null;
        //imageSprite.color = new Color32(255,255,255,0);
        //HostTransform = dashboardManager.CardHolder.transform;
        HostTransform.position = this.transform.position;
        HostTransform.rotation = this.transform.rotation;

        isDragging = true;
        Transform cameraTransform = CameraCache.Main.transform;

        currentInputSource = eventData.InputSource;
        currentInputSourceId = eventData.SourceId;


        eventData.Use(); // Mark the event as used, so it doesn't fall through to other handlers.
        currentInputSource = eventData.InputSource;
        currentInputSourceId = eventData.SourceId;

        HostTransform.position = new Vector3(HostTransform.transform.position.x, HostTransform.transform.position.y, this.transform.position.z);
        Vector3 initialDraggingPosition = HostTransform.position;
        InputManager.Instance.PushModalInputHandler(gameObject);

        Vector3 inputPosition = Vector3.zero;
        InteractionSourceInfo sourceKind;
        currentInputSource.TryGetSourceKind(currentInputSourceId, out sourceKind);
        switch (sourceKind)
        {
            case InteractionSourceInfo.Hand:
                currentInputSource.TryGetGripPosition(currentInputSourceId, out inputPosition);
                break;
            case InteractionSourceInfo.Controller:
                currentInputSource.TryGetPointerPosition(currentInputSourceId, out inputPosition);
                break;
        }


        Vector3 handPivotPosition = GetHandPivotPosition(cameraTransform);
        Vector3 objDirection = Vector3.Normalize(HostTransform.position - handPivotPosition);
        Vector3 handDirection = Vector3.Normalize(inputPosition - handPivotPosition);
        objDirection = cameraTransform.InverseTransformDirection(objDirection);
        handDirection = cameraTransform.InverseTransformDirection(handDirection);

        gazeAngularOffset = Quaternion.FromToRotation(handDirection,objDirection);

        OnCardSelect.Invoke();
    }


    void IInputHandler.OnInputUp(InputEventData eventData)
    {
        if (!isDragging) return;
        isDragging = false;
        InputManager.Instance.PopModalInputHandler();
        OnCardPlace.Invoke();
        //dashboardManager.FindClosestCard(dashboardManager);
    }

    void ISourceStateHandler.OnSourceDetected(SourceStateEventData eventData)
    {
    }

    void ISourceStateHandler.OnSourceLost(SourceStateEventData eventData)
    {
        if (!isDragging) return;
        isDragging = false;
        InputManager.Instance.PopModalInputHandler();
        OnCardPlace.Invoke();

    }

    // Use this for initialization
    void Start()
    {

        imageSprite = GetComponentInChildren<Image>();
    }

    // Update is called once per frame
    void Update()
    {

        UpdateDragging();
    }

    private Vector3 GetHandPivotPosition(Transform cameraTransform)
    {
        return cameraTransform.position + new Vector3(0, -0.2f, 0) - cameraTransform.forward * 0.2f; // a bit lower and behind
    }

    private void UpdateDragging()
    {
        if (!isDragging) return;
        Transform cameraTransform = CameraCache.Main.transform;

        Vector3 inputPosition = Vector3.zero;
        InteractionSourceInfo sourceKind;

        currentInputSource.TryGetSourceKind(currentInputSourceId, out sourceKind);
        switch (sourceKind)
        {
            case InteractionSourceInfo.Hand:
                currentInputSource.TryGetGripPosition(currentInputSourceId, out inputPosition);
                break;
            case InteractionSourceInfo.Controller:
                currentInputSource.TryGetPointerPosition(currentInputSourceId, out inputPosition);
                break;
        }


        Vector3 pivotPosition = GetHandPivotPosition(cameraTransform);

        Vector3 newHandDirection = Vector3.Normalize(inputPosition - pivotPosition);

        newHandDirection = cameraTransform.InverseTransformDirection(newHandDirection); // in camera space

        Vector3 targetDirection = Vector3.Normalize(gazeAngularOffset * newHandDirection);
        targetDirection = cameraTransform.TransformDirection(targetDirection); // back to world space

        RaycastHit hit;
        bool isHit = Physics.Raycast(pivotPosition, targetDirection, out hit,5f);
        if (isHit)
        {
            Vector3 newPosition = new Vector3(hit.point.x, hit.point.y, hit.point.z)+ this.transform.forward * (-0.1f);
            HostTransform.position = Vector3.Lerp(HostTransform.position, newPosition, .1f);
        }
        else
        {

        }

  
    }

    private void GetClosestPoints()
    {
        Vector3 center = bounds.center;
        Vector3 min1 = bounds.center - bounds.extents;
        Vector3 max1 = bounds.center + bounds.extents;
        Vector3 pt1 = new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z);
        Vector3 pt2 = new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z);

        RaycastHit hitInfo;
        Physics.Raycast(min1, Vector3.forward, out hitInfo, 5f);

    }
}
