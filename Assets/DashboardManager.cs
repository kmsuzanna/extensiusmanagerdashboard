﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HoloToolkit.Unity;

public class DashboardManager : MonoBehaviour
{
    public bool isFollowing;
    struct Card
    {
        public GameObject CardGameObject;
    }
    [Tooltip("The physics layer that defines the area a card can slide to.")]
    public int DashboardLayer = 8;

    [Tooltip("The physics layer that defines the area a card can slide to.")]
    public Image ImageCardBeingPlaced;
    public GameObject CardBeingPlacedContainer;
    public GameObject CardPrefab;

    public GameObject CardParents;

    private GameObject originalCard;
    private GameObject currentCard;

    private bool isPopulated;

    public Sprite DoneMovingCard()
    {
        Sprite sprite = ImageCardBeingPlaced.sprite;

        CardBeingPlacedContainer.SetActive(false);
        return sprite;
    }

    public void InitMovingCard(Vector3 position, Sprite sprite, GameObject card)
    {
        originalCard = card;
        CardBeingPlacedContainer.SetActive(true);
        CardBeingPlacedContainer.transform.position = position;
        ImageCardBeingPlaced.sprite = sprite;
    }


    private int MaxCards = 16;
    private Dictionary<int, Card> mapCards;
    // Use this for initialization
    void Start()
    {
        isPopulated = false;
        mapCards = new Dictionary<int, Card>();

        for (int i = 0; i < MaxCards; i++)
        {
            Card card = new Card();
            GameObject cardObject = Instantiate(CardPrefab, CardParents.transform);
            cardObject.name = "Card_" + i;
            card.CardGameObject = cardObject;
            CardController cc = cardObject.GetComponentInChildren<CardController>();
            cc.Initialize(this);
            CardPlaceable cp = cardObject.GetComponentInChildren<CardPlaceable>();
            mapCards[i] = card;
            //card.CardGameObject = cardObject;
        }

    }

    
    public void PopulateDashboard()
    {
        Debug.Log("PopulateDashboard");
        if (isPopulated) return;
        isPopulated = true;

        for (int i = 0; i < CardManager.Instance.CardDatabase.Length; i++)
        {
            GameObject card = mapCards[i].CardGameObject;
            CardController cc = card.GetComponentInChildren<CardController>();
            cc.AddCard(CardManager.Instance.CardDatabase[i]);
        }
    }

    public void ClearDashboard()
    {
        Debug.Log("ClearDashboard");

        if (!isPopulated) return;

        for (int i = 0; i < MaxCards; i++)
        {
            Card card = mapCards[i];
            GameObject cardObject = card.CardGameObject;
            CardController cc = cardObject.GetComponentInChildren<CardController>();
            cc.RemoveCard();
        }

        isPopulated = false;
    }

    public void Follow()
    {
        isFollowing = true;
        Tagalong tg = GetComponentInChildren<Tagalong>();
        tg.enabled = true;
        Billboard bb = GetComponentInChildren<Billboard>();
        bb.enabled = true;


        for (int i = 0; i < MaxCards; i++)
        {
            Card card = mapCards[i];
            GameObject cardObject = card.CardGameObject;
            CardController cc = cardObject.GetComponentInChildren<CardController>();
            cc.SetSelectable(false);
        }
    }

    public void PlaceDashboard()
    {
        isFollowing = false;

        Tagalong tg = GetComponentInChildren<Tagalong>();
        tg.enabled = false;
        Billboard bb = GetComponentInChildren<Billboard>();
        bb.enabled = false;

        FixedAngularSize fa = GetComponentInChildren<FixedAngularSize>();
        if (fa != null)
            fa.enabled = false;

        for (int i = 0; i < MaxCards; i++)
        {
            Card card = mapCards[i];
            GameObject cardObject = card.CardGameObject;
            CardController cc = cardObject.GetComponentInChildren<CardController>();
            cc.SetSelectable(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
